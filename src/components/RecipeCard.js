import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import Card, {CardActions, CardContent, CardMedia} from 'material-ui/Card';

const styles = {
  card: {
    maxWidth: 345
  },
  media: {
    height: 200
  }
};

function RecipeCard(props) {
  const {classes, recipe} = props;
  return (
    <Card className={classes.card}>
      <CardMedia className={classes.media} image={recipe.mainImageUrl}/>
      <CardContent>
        <Typography type="title" component="h2">
          {recipe.title}
        </Typography>
        <Typography type="caption" component="p">
          By {recipe.userName}
        </Typography>
      </CardContent>
    </Card>
  )
}

RecipeCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RecipeCard);