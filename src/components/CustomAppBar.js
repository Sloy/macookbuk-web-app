import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import IconButton from 'material-ui/IconButton';
import PhoneAndroid from 'material-ui-icons/PhoneAndroid'

const styles = {
  root: {
    width: '100%'
  },
  flex: {
    flex: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

function CustomAppBar(props) {
  const {classes} = props;
  return (
    <AppBar
      position="static"
      color="primary"
      title="MaCookBuk"
      iconClassNameRight="muidocs-icon-navigation-expand-more">
      <Toolbar >
        <Typography className={classes.flex} type="title" color="inherit">
          MaCookBuk
        </Typography>
        <IconButton
          component="a"
          color="inherit"
          href="https://play.google.com/store/apps/details?id=com.sloydev.macookbuk">
          <PhoneAndroid/>
        </IconButton>
      </Toolbar>
    </AppBar>
  )
}

CustomAppBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(CustomAppBar)