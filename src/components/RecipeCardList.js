import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Grid from 'material-ui/Grid';
import RecipeCard from './RecipeCard'

const styles = theme => ({
  root: {
    justifyContent: 'space-around',
    flexGrow: 1
  }
});

function RecipeCardList(props) {
  const {classes, recipes} = props;
  return (
    <div className={classes.root}>
      <Grid container spacing={16}>
        {recipes.map(it => {
          return (
            <Grid item>
              <RecipeCard recipe={it}/>
            </Grid>
          )
        })}
      </Grid>
    </div>
  )
}

RecipeCardList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RecipeCardList)