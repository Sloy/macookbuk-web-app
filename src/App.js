import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Button from 'material-ui/Button';
import Typography from 'material-ui/Typography';
import CustomAppBar from './components/CustomAppBar'
import RecipeCardList from './components/RecipeCardList'

const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

var config = {
  apiKey: "AIzaSyCRpuXDh6Dub7SYS6Zme0_IViFDijdJxik",
  authDomain: "macookbuk.firebaseapp.com",
  databaseURL: "https://macookbuk.firebaseio.com",
  projectId: "macookbuk",
  storageBucket: "macookbuk.appspot.com",
  messagingSenderId: "935754979503"
};
firebase.initializeApp(config);
// Initialize Cloud Firestore through Firebase
var db = firebase.firestore();

class App extends Component {
  state = {
    recipes: []
  }

  componentDidMount() {
    db
      .collection("recipes")
      .where("public", "==", true)
      .get()
      .then((querySnapshot) => querySnapshot.docs.map(doc => doc.data()))
      .then((recipes) => {
        this.setState({recipes: recipes})
      })
  }

  render() {
    return (
      <MuiThemeProvider>
        <CustomAppBar/>
        <h1>Hello world</h1>
        <RecipeCardList recipes={this.state.recipes}/>
      </MuiThemeProvider>
    );
  }
}

export default App;
